# Tareas del curso de herramientas informaticas



## Estudiante: Bruk Wilian Pancca Mamani
#### Tareas Para Casa
- [Ejercicio 1](<Tareas/EJERCICIO 01_VENTAS.xlsx>)
- [Ejercicio 2](<Tareas/EJERCICIO 02_FACTURA.xlsx>)
- [Ejercicio 3](<Tareas/EJERCICIO 03_MOVIMIENTOS.xlsx>)
- [Ejercicio 4](<Tareas/EJERCICIO 04_VENTAS-GRAFICOS.xlsx>)
- [Ejercicio 5](<Tareas/EJERCICIO 05_LIQUIDACION.xlsx>)
- [Ejercicio 9](<Tareas/EJERCICIO 09_DEPARTAMENTOS.xlsx>)
#### Practicas del laboratorio
- [Practica 11 "Graficos de una Variable](<Practicas Del Laboratorio/11.Graficos+de+una+Variable.xlsx>)
- [ Practica 12 "Graficos de dos o mas variables"](<Practicas Del Laboratorio/12.Graficos+de+dos+o+mas+variables.xlsx>)
- [ Practica 13 "Teoria concepto base datos"](<Practicas Del Laboratorio/13.Teoria+Concepto+Base+Datos.xlsx>)
- [ Practica 14 "Orden de datos"](<Practicas Del Laboratorio/14.Orden+de+Datos.xlsx>)
- [Practica Calificada 1](<Practicas Del Laboratorio/EMPLEADOS.xlsm>)
- [Practica Calificada 2](<Practicas Del Laboratorio/Practica calificada2.xlsm>)